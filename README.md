# Open Software Base (in YAML format)
This is the Open Software Base is an experimentation from [Etalab](https://www.etalab.gouv.fr/), the French Prime Minister task force for open data.

This database is built from various sources. It does not aim at replacing other database or program lists, but to use and synthesize their data. It is open to users contributions.

The database can be explored on a web interface at (code.gouv2.fr)[[https://code.gouv.fr](https://code.gouv.fr)]. You can browse and contribute to the [code of this explorer](https://git.framasoft.org/codegouv/open-software-base-explorer).

The main effort on this project is put on adding more and more sources to the database. All the informations gathered should allow to build an list of programs and their objective characteristics, which can be used with confidence by users in order to evaluate software solutions.

## Sources
The data from each source is kept in YAML its own git repository in order to preserve history. Another repository contains the script which extract the data from the source. We do not impose structure on the data. As much as possible, we preserve the original structure of the source in order to never destroy data in the process.

The script [merge-open-software-base-yaml](https://git.framasoft.org/codegouv/merge-open-software-base-yaml) generate the present repository from all the other source repositories. This script generates a YAML file for each program, with the raw data from each source written under a key named after this source. It also generates a `canonical` key which synthesizes data from various sources based on predefined priorities. The best explanation is [an example of such a file](https://git.framasoft.org/codegouv/open-software-base-yaml/blob/master/cups.yaml).

### Sources list

All the sources repository are listed with their last updated date on [this page](https://code.gouv2.fr/updated).

#### [Appstream Debian](https://wiki.debian.org/AppStream)

* Script: https://git.framasoft.org/codegouv/appstream-debian-to-yaml
* Data: https://git.framasoft.org/codegouv/appstream-debian-yaml

#### [Civicstack](http://www.civicstack.org/)

* Script: https://git.framasoft.org/codegouv/civicstack-to-yaml
* Data: https://git.framasoft.org/codegouv/civicstack-yaml

#### [Nuit Debout](https://wiki.nuitdebout.fr/wiki/Ressources/Liste_d%27outils_num%C3%A9riques)

* Script: https://git.framasoft.org/codegouv/nuit-debout-to-yaml
* Data: https://git.framasoft.org/codegouv/nuit-debout-yaml

#### OGP Toolbox Framacalc [tools](https://framacalc.org/ogptoolbox) & [usages](https://framacalc.org/usages_ogptoolbox)

* Script: https://git.framasoft.org/codegouv/ogptoolbox-framacalc-to-yaml
* Data: https://git.framasoft.org/codegouv/ogptoolbox-framacalc-yaml

#### [ParticipateDB](http://www.participatedb.com/)

* Script: https://git.framasoft.org/codegouv/participatedb-to-yaml
* Data: https://git.framasoft.org/codegouv/participatedb-yaml

#### [Tech Platforms for Civic-Participation](https://docs.google.com/spreadsheets/d/1YBZLdNsGohGBjO5e7yrwOQx78IzCA6SNW6T14p15aKU)

* Script: https://framagit.org/codegouv/tech-plateforms-to-yaml
* Data: https://framagit.org/codegouv/tech-plateforms-yaml

#### [Ultimate Debian Database](https://wiki.debian.org/UltimateDebianDatabase)

* Script: https://git.framasoft.org/codegouv/udd-to-yaml
* Data: https://git.framasoft.org/codegouv/udd-yaml

#### [WikiData](https://www.wikidata.org/wiki/Wikidata:Main_Page)

* Script: https://git.framasoft.org/codegouv/wikidata-to-yaml
* Data: https://git.framasoft.org/codegouv/wikidata-yaml


#### Contributions

The Open Software Base is also opened to programs added by users which are not present in either automated sources. This source, as the other ones, takes the form of a git repository containing YAML files. You are invited to fork this repository and create merge request in order to add informations. Merge request are validated by a script which check that basic informations are present.

* Validator: https://git.framasoft.org/codegouv/contributions-validator
* Data: https://git.framasoft.org/codegouv/contributions-yaml

#### Any source you want to add

You can contribute to the project by creating a script to extract data from a new source. Get in touch with us on [Etalab forum](https://forum.etalab.gouv.fr/t/projet-de-catalogue-de-logiciels/38/17) !
